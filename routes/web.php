<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::prefix('admin')->namespace('Admin')->group(function () {
    Route::any('login', 'UserController@login')->name('admin-login');
    
    Route::group(['middleware' => 'adminAuth'], function(){
    Route::get('/', 'DashboardController@index')->name('admin');
    Route::any('logout', 'UserController@logout')->name('admin-logout');
    Route::get('dashboard', 'DashboardController@index')->name('admin-dashboard');
    
    Route::resource('clients', 'ClientController');
    Route::resource('projects', 'ProjectController');
    Route::resource('companies', 'CompanyController');
    Route::resource('users', 'UserController');
    Route::resource('branches', 'BranchController');
    Route::resource('profiles', 'ProfileController');
    Route::get('getbranches/{id}', 'BranchController@getBranches')->name('admin-ajax-branches');
    Route::get('getclients/{id}', 'ClientController@getClients')->name('admin-ajax-cleints');

    

    Route::bind('page', function($value, $route) {
        return App\Models\Page::find($value);
    });

    Route::resource('pages', 'PageController', [
        'names' => [
            'edit' => 'page.edit',
            'show' => 'page.show',
            'destroy' => 'page.destroy',
            'update' => 'page.update',
            'store' => 'page.store',
            'index' => 'page',
            'create' => 'page.create',
        ]
            ]
    );
     });
});
Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

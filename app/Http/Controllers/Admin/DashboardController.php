<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

class DashboardController extends AdminBaseController {

    public function __construct(Request $request) {
        parent::__construct($request);
    }

    public function index(){   
     return parent::output('dashboard');   
    }
}

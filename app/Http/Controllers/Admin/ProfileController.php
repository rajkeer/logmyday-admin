<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\ProfileRequest;
use Illuminate\Support\Facades\Redirect;
use Input;
use Validator;
use Auth;
use Paginate;
use HTML;
use Form;
use Hash;
use View;
use URL;
use Session;
use Route;
use Crypt;
use Response;
use App\Models\Profile;

class ProfileController extends AdminBaseController {


    public function __construct(Request $request) {
        parent::__construct($request);

    }
    
    public function index(Request $request){
        $user = Auth::user();
        $profile = Profile::with('address')->with('emails')->with('phones')->with('projects')->where('user_id',$user->id)->first();
        if(!$profile){
           return Redirect::to(route('profiles.create'));
        }
        
        return parent::output('profiles.index',  compact('user','profile'));
    }
    

    public function create(Profile $profile) {
        $heading_title = 'Create Profile';
        return parent::output('profiles.create', compact('profile', 'heading_title'));
    }

    public function store(ProfileRequest $request, Profile $profile) {
        $profile->fill(Input::all());
        $profile->save();

        return Redirect::to(route('profiles.index'))
                        ->with('flash_alert_notice', 'New profile  successfully created!');
    }

    public function edit(Profile $profile) {
        $heading_title = 'Edit Profile';

        return parent::output('profiles.edit', compact('profile', 'heading_title'));
    }

    public function update(ProfileRequest $request, Profile $profile) {
        $profile->fill(Input::except(['address']));
        $profile->save();
        return Redirect::to(route('profiles.index'))
                        ->with('flash_alert_notice', 'Profile successfully updated.');
    }

    public function destroy(Profile $profile) {

        $profile->status = 2;
        $profile->save();
        return Redirect::to(route('profiles.index'))
                        ->with('flash_alert_notice', 'Profile successfully deleted.');
    }

    public function show(Profile $profile) {
        $heading_title = 'Profile Detail';
        return parent::output('profiles.show', compact('profile', 'heading_title'));
    }


}

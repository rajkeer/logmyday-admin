<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\BranchRequest;
use Illuminate\Support\Facades\Redirect;
use Input;
use Validator;
use Auth;
use Paginate;
use HTML;
use Form;
use Hash;
use View;
use URL;
use Session;
use Route;
use Crypt;
use Response;
use App\Models\Branch;
use App\Models\Company;

class BranchController extends AdminBaseController {

    public function __construct(Branch $branch, Request $request) {
        parent::__construct($request);
        $companies = Company::select('id', 'name')->pluck('name', 'id')->toArray();
        View::share(compact('companies'));
    }

    public function index(Branch $branch, Request $request) {
        $heading_title = 'Branch';
        $button_add = 'Add Branch';

        $q = $request->get('search');
        $branch = $branch->with('company')->with('address');
        if ($q) {
            $results = $branch->where('name', 'LIKE', "%$q%")->Paginate($this->limit);
        } else {
            $results = $branch->Paginate($this->limit);
        }

        return parent::output('branches.index', compact('results', 'heading_title', 'button_add'));
    }

    public function create(Branch $branch) {

        $companies = Company::select('id', 'name')->pluck('name', 'id')->toArray();

        $heading_title = 'Create Branch';
        return parent::output('branches.create', compact('branch', 'heading_title', 'companies'));
    }

    public function store(BranchRequest $request, Branch $branch) {
        $addressData = Input::all('address');
        $address_id = null;
        $address = null;
        if (isset($addressData['address'])) {
            if (isset($branch->address_id))
                $address = \App\Models\Address::find($branch->address_id);
            if (!$address)
                $address = new \App\Models\Address;
            $address->fill($addressData['address']);
            $address->save();
            $address_id = $address->id;
        }
        $branch->fill(Input::all());
        unset($branch->address);
        $branch->address_id = $address_id;
        $branch->save();

        return Redirect::to(route('branches.index'))
                        ->with('flash_alert_notice', 'New branch  successfully created!');
    }

    public function edit(Branch $branch) {
        $companies = Company::select('id', 'name')->pluck('name', 'id')->toArray();
        $heading_title = 'Edit Branch';

        return parent::output('branches.edit', compact('branch', 'heading_title', 'companies'));
    }

    public function update(BranchRequest $request, Branch $branch) {
        $addressData = Input::all('address');
        $address_id = null;
        $address = null;
        if (isset($addressData['address'])) {
            if (isset($branch->address_id) && $branch->address_id)
                $address = \App\Models\Address::find($branch->address_id);
            if (!$address)
                $address = new \App\Models\Address;

            $address->fill($addressData['address']);
            $address->save();
            $address_id = $address->id;
        }
        $branch->fill(Input::all());
        unset($branch->address);
        $branch->address_id = $address_id;
        $branch->save();
        return Redirect::to(route('branches.index'))
                        ->with('flash_alert_notice', 'Branch successfully updated.');
    }

    public function destroy(Branch $branch) {

        $branch->status = 2;
        $branch->save();
        return Redirect::to(route('branches.index'))
                        ->with('flash_alert_notice', 'Branch successfully deleted.');
    }

    public function show(Branch $branch) {
        $heading_title = 'Branch Info';

        return parent::output('branches.show', compact('branch', 'heading_title'));
    }

    public function getBranches($company_id = 0) {
        $branches = Branch::select('id', 'name')->where('company_id', $company_id)->pluck('name', 'id')->toArray();
        return $branches;
    }

}

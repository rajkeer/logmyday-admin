<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\ClientRequest;
use Illuminate\Support\Facades\Redirect;
use Input;
use Validator;
use Auth;
use Paginate;
use HTML;
use Form;
use Hash;
use View;
use URL;
use Session;
use Route;
use Crypt;
use Response;
use App\Models\Client;
use App\Models\Company;

class ClientController extends AdminBaseController {

    public function __construct(Client $client, Request $request) {
        parent::__construct($request);
        $companies = Company::select('id', 'name')->pluck('name', 'id')->toArray();
        View::share(compact('companies'));
    }

    public function index(Client $client, Request $request) {
        $heading_title = 'Client';
        $button_add = 'Add Client';

        $q = $request->get('search');
        $client = $client->with('company');
        if ($q) {
            $results = $client->where('name', 'LIKE', "%$q%")->Paginate($this->limit);
        } else {
            $results = $client->Paginate($this->limit);
        }

        return parent::output('clients.index', compact('results', 'heading_title', 'button_add'));
    }

    public function create(Client $client) {
        $heading_title = 'Create Client';
        return parent::output('clients.create', compact('client', 'heading_title'));
    }

    public function store(ClientRequest $request, Client $client) {
        $client->fill(Input::all());
        $client->save();

        return Redirect::to(route('clients.index'))
                        ->with('flash_alert_notice', 'New client  successfully created!');
    }

    public function edit(Client $client) {
        $heading_title = 'Edit Client';

        return parent::output('clients.edit', compact('client', 'heading_title'));
    }

    public function update(ClientRequest $request, Client $client) {
        $client->fill(Input::all());
        $client->save();
        return Redirect::to(route('clients.index'))
                        ->with('flash_alert_notice', 'Client successfully updated.');
    }

    public function destroy(Client $client) {

        $client->status = 2;
        $client->save();
        return Redirect::to(route('clients.index'))
                        ->with('flash_alert_notice', 'Client successfully deleted.');
    }

    public function show(Client $client) {
        $heading_title = 'Client Detail';
        return parent::output('clients.show', compact('client', 'heading_title'));
    }

    public function getClients($company_id = 0) {
        $clients = Client::select('id', 'name')->where('company_id', $company_id)->pluck('name', 'id')->toArray();
        return $clients;
    }

}

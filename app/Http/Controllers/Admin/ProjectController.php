<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\ProjectRequest;
use Illuminate\Support\Facades\Redirect;
use Input;
use Validator;
use Auth;
use Paginate;
use HTML;
use Form;
use Hash;
use View;
use URL;
use Session;
use Route;
use Crypt;
use Response;
use App\Models\Project;
use App\Models\Company;
use App\Models\Branch;

class ProjectController extends AdminBaseController {

    public function __construct(Project $project, Request $request) {
        parent::__construct($request);
        $companies = Company::select('id', 'name')->pluck('name', 'id')->toArray();
        View::share(compact('companies'));
    }

    public function index(Project $project, Request $request) {
        $heading_title = 'Project';
        $button_add = 'Add Project';

        $q = $request->get('search');
        $project = $project->with('branch')->with('client');
        if ($q) {
            $results = $project->where('name', 'LIKE', "%$q%")->Paginate($this->limit);
        } else {
            $results = $project->Paginate($this->limit);
        }

        return parent::output('projects.index', compact('results', 'heading_title', 'button_add'));
    }

    public function create(Project $project) {
        $heading_title = 'Create Project';
        $branch = Branch::find($project->company_branch_id);
        $company_id = 0;
        $branch_id = 0;
        if ($branch && $branch->count()) {
            $company_id = $branch->company_id;
        }
        return parent::output('projects.create', compact('project', 'company_id', 'branch_id', 'heading_title'));
    }

    public function store(ProjectRequest $request, Project $project) {
        $project->fill(Input::except('company_id'));
        $project->save();

        return Redirect::to(route('projects.index'))
                        ->with('flash_alert_notice', 'New project  successfully created!');
    }

    public function edit(Project $project) {
        $heading_title = 'Edit Project';
        $branch = Branch::find($project->company_branch_id);
        $company_id = 0;
        $branch_id = 0;
        if ($branch &&$branch->count()) {
            $company_id = $branch->company_id;
        }

        return parent::output('projects.edit', compact('project', 'company_id', 'branch_id', 'heading_title'));
    }

    public function update(ProjectRequest $request, Project $project) {
        $project->fill(Input::except('company_id'));
        $project->save();
        return Redirect::to(route('projects.index'))
                        ->with('flash_alert_notice', 'Project successfully updated.');
    }

    public function destroy(Project $project) {

        $project->status = 2;
        $project->save();
        return Redirect::to(route('projects.index'))
                        ->with('flash_alert_notice', 'Project successfully deleted.');
    }

    public function show(Project $project) {
        $heading_title = 'Project Info';
        return parent::output('projects.show', compact('project', 'heading_title'));
    }

}

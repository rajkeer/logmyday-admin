<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\CompanyRequest;
use Illuminate\Support\Facades\Redirect;
use Input;
use Validator;
use Auth;
use Paginate;
use HTML;
use Form;
use Hash;
use View;
use URL;
use Session;
use Route;
use Crypt;
use Response;
use App\Models\Company;

class CompanyController extends AdminBaseController {
    public function __construct(Company $company, Request $request) {
        parent::__construct($request);
        //$this->middleware('admin');
    }
    public function index(Company $company, Request $request) {
        $heading_title = 'Company';
        $button_add = 'Add Company';

        $q = $request->get('search');
        if ($q) {
            $results = $company->where('name', 'LIKE', "%$q%")->Paginate($this->limit);
        } else {
            $results = $company->Paginate($this->limit);
        }

        return parent::output('companies.index', compact('results', 'heading_title', 'button_add'));
    }

    public function create(Company $company) {
        $heading_title = 'Create Company';
        return parent::output('companies.create', compact('company', 'heading_title'));
    }

    public function store(CompanyRequest $request, Company $company) {
        $company->fill(Input::all());
        $company->save();

        return Redirect::to(route('companies.index'))
                        ->with('flash_alert_notice', 'New company  successfully created!');
    }

    public function edit(Company $company) {
        $heading_title = 'Edit Company';

        return parent::output('companies.edit', compact('company', 'heading_title'));
    }

    public function update(CompanyRequest $request, Company $company) {
        $company->fill(Input::all());
        $company->save();
        return Redirect::to(route('companies.index'))
                        ->with('flash_alert_notice', 'Company successfully updated.');
    }

    public function destroy(Company $company) {

        $company->status=2;
        $company->save();
        return Redirect::to(route('companies.index'))
                        ->with('flash_alert_notice', 'Company successfully deleted.');
    }

    public function show(Company $company) {
        $heading_title = 'Company Info';

        return parent::output('companies.show', compact('company', 'heading_title'));
    }

}

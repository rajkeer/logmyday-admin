<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Input;
use Validator;
use Auth;
use Paginate;
use HTML;
use Form;
use Hash;
use View;
use URL;
use Session;
use Route;
use Crypt;
use Response;
use App\Http\Requests\UserRequest;
use App\Models\User;
use App\Models\UserType;

class UserController extends AdminBaseController {

    public function __construct(Request $request) {
        parent::__construct($request);
    }

    public function login(Request $request) {

        if ($request->isMethod('POST')) {
            $this->loginProccess($request);
        }
       if ( Auth::check() && Auth::user()->isAdmin() ) {
            return redirect('admin/dashboard');
        }
        
        return parent::output('login');
    }

    private function loginProccess(Request $request) {
        if (Auth::attempt(array(
                    'username' => $request->get('username'),
                    'password' => $request->get('password'),
                    'is_admin' => 1,//TODO- SETUP-ADMIN
                ))) {
            session([
                'name' => $request->get('username')
            ]);
            return redirect('admin/dashboard');
        } else {
            Session::flash('message', "Invalid Credentials , Please try again.");
            return Redirect::back();
        }
    }

    public function logout() {
        Auth::logout();
        Session::flush();
        return redirect('admin/login');
    }

    public function index(User $user, Request $request) {
        $heading_title = 'User';
        $button_add = 'Add User';

        $q = $request->get('search');
        $status = $request->get('status');
        $user_type = $request->get('user_type');
        if ($q) {
            $user = $user->where('username', 'LIKE', "%$q%");
        }
        if ($status) {
            $user = $user->where('status', (int) $status);
        }
        if ($user_type) {
            $user = $user->where('user_type', (int) $user_type);
        }
        $results = $user->Paginate($this->limit);
        $user_types = array('' => '--- Any Role --') + UserType::select('id', 'name')->pluck('name', 'id')->toArray();


        return parent::output('users.index', compact('results', 'heading_title', 'button_add', 'user_types', 'user_type', 'status'));
    }

    public function create(User $user) {
        $heading_title = 'Create User';
        $user_types = UserType::select('id', 'name')->pluck('name', 'id')->toArray();

        return parent::output('users.create', compact('user', 'heading_title', 'user_types'));
    }

    public function store(UserRequest $request, User $user) {
        $user->fill(Input::all());
        $newPassword = $user->new_password;
        unset($user->new_password);
        // $user->user_type=3;//DEVELOPER
        $user->password = bcrypt($newPassword);
        $user->save();

        return Redirect::to(route('users.index'))
                        ->with('flash_alert_notice', 'New project  successfully created!');
    }

    public function edit(User $user) {
        $heading_title = 'Edit User';
        $user_types = UserType::select('id', 'name')->pluck('name', 'id')->toArray();

        return parent::output('users.edit', compact('user', 'heading_title', 'user_types'));
    }

    public function update(UserRequest $request, User $user) {
        $user->fill(Input::all());
        $newPassword = $user->new_password;
        unset($user->new_password);
        if ($newPassword)
            $user->password = bcrypt($newPassword);

        $user->save();
        return Redirect::to(route('users.index'))
                        ->with('flash_alert_notice', 'User successfully updated.');
    }

    public function destroy(User $user) {

        //User::where('id', $user->id)->delete();
        $user->status = 2;
        $user->save();
        return Redirect::to(route('users.index'))
                        ->with('flash_alert_notice', 'User successfully deleted.');
    }

    public function show(User $user) {
        $heading_title = 'User Detail';
        return parent::output('users.show', compact('user', 'heading_title'));
    }

}

<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Input;
use Validator;
use Auth;
use Paginate;
use HTML;
use Form;
use Hash;
use View;
use URL;
use Session;
use Route;
use Crypt;
use Response;
use App\Http\Requests\PageRequest;
use App\Models\Page;

/**
 * Class AdminController
 */
class PageController extends AdminBaseController {
    public function __construct(Page $contact,Request $request) {
        parent::__construct($request);
        //$this->middleware('admin');
    }

    public function index(Page $contact, Request $request) {
        $heading_title = 'Pages';
        $button_add = 'Add Page';

        $q = $request->get('search');
        if ($q) {
            $results = $contact->where('page_title', 'LIKE', "%$q%")->Paginate($this->limit);
        } else {
            $results = $contact->Paginate($this->limit);
        }

        return parent::output('pages.index', compact('results', 'heading_title','button_add'));
    }

    public function create(Page $page) {

        return parent::output('pages.create', compact('page'));
    }

    public function store(PageRequest $request, Page $page) {
        $page->fill(Input::all());
        $page->save();

        return Redirect::to(route('page'))
                        ->with('flash_alert_notice', 'New program  successfully created!');
    }

    public function edit(Page $page) {
        return parent::output('pages.edit', compact('page'));
    }

    public function update(PageRequest $request, Page $page) {
        $page->fill(Input::all());
        $page->save();
        return Redirect::to(route('page'))
                        ->with('flash_alert_notice', 'Page successfully updated.');
    }

    public function destroy(Page $page) {

        $page->status=2;
        $page->save();
        return Redirect::to(route('page'))
                        ->with('flash_alert_notice', 'Page successfully deleted.');
    }

    public function show(Page $page) {
        $heading_title = 'Page Detail';
        return parent::output('pages.show', compact('page', 'heading_title'));
    }

}

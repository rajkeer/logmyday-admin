<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Response;
use Auth;

class AdminBaseController extends Controller {

    protected $errors = array();
    protected $currentUserId = 0;
    static public $default_user_type = 3; //DEVELOPER
    public $limit = 20;
    static public $default_tasklog_status = 1;
    public $title ='Administrator | LogMyDay';
    public $meta_description ='Administrator | LogMyDay';
    public $meta_keyword ='Administrator | LogMyDay';
    public $meta_author ='LMD';
    public $heading_title ='Administrator | LogMyDay';

    public function __construct(Request $request) {
        $defautl_title =\Request::segment(2);
       if($defautl_title) {
       $this->title = ucfirst($defautl_title)." | LogMyDay";
       $this->heading_title = ucfirst($defautl_title);
       }
    }

    public function output($view, $data = []) {
        $data['theme_admin_layout'] = 'admin.layout.master';
        $data['title'] = $this->title;
        $data['heading_title'] = $this->heading_title;
        $data['meta_description'] = $this->meta_keyword;
        $data['meta_keyword'] = $this->meta_description;
        $data['meta_author'] = $this->meta_author;
        return view('admin.'.$view,$data);
    }

}

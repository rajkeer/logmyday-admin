<?php

namespace App\Http\Requests;

use App\Http\Requests\Request; 
 

class PageRequest  extends Request {

    /**
     * The product validation rules.
     *
     * @return array
     */
    public function rules() { 
            switch ( $this->method() ) {

                case 'GET':
                case 'DELETE': {
                        return [ ];
                    }
                case 'POST': {
                        return [
                            'page_title'             => 'required'
                        ];
                    }
                case 'PUT':
                case 'PATCH': {

                    if ( $page = $this->page ) {

                        return [
                            'page_title'             => 'required' 
                            ];
                    }
                }
                default:break;
            }
        //}
    }

    /**
     * The
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

}

<?php

namespace App\Http\Requests;

use App\Http\Requests\Request; 
 

class ProfileRequest  extends Request {

    /**
     * The product validation rules.
     *
     * @return array
     */
    public function rules() { 
            switch ( $this->method() ) {

                case 'GET':
                case 'DELETE': {
                        return [ ];
                    }
                case 'POST': {
                        return [
                            'username'             => 'required',
                            'mobile'             => 'required|unique:users',
                            'email'             => 'required|unique:users|email'
                        ];
                    }
                case 'PUT':
                case 'PATCH': {

                    if ( $profile = $this->profile ) {

                        return [
                            'first_name'             => 'required' ,
                            'last_name'             => 'required'
                            ];
                    }
                }
                default:break;
            }
        //}
    }

    /**
     * The
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

}

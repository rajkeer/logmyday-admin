<?php

namespace App\Http\Requests;

use App\Http\Requests\Request; 
 

class ProjectRequest  extends Request {

    /**
     * The product validation rules.
     *
     * @return array
     */
    public function rules() { 
            switch ( $this->method() ) {

                case 'GET':
                case 'DELETE': {
                        return [ ];
                    }
                case 'POST': {
                        return [
                            'name'             => 'required',
                            'description'             => 'required'
                        ];
                    }
                case 'PUT':
                case 'PATCH': {

                    if ( $project = $this->project ) {

                        return [
                            'name'             => 'required' ,
                            'description'             => 'required' 
                            ];
                    }
                }
                default:break;
            }
        //}
    }

    /**
     * The
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

}

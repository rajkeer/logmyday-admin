<?php

namespace App\Http\Requests;

use App\Http\Requests\Request; 
 

class BranchRequest  extends Request {

    /**
     * The product validation rules.
     *
     * @return array
     */
    public function rules() { 
            switch ( $this->method() ) {

                case 'GET':
                case 'DELETE': {
                        return [ ];
                    }
                case 'POST': {
                        return [
                            'name'             => 'required'
                        ];
                    }
                case 'PUT':
                case 'PATCH': {

                    if ( $branch = $this->branch ) {

                        return [
                            'name'             => 'required' ,
                            //'address[additional_details]'             => 'required' ,
                            ];
                    }
                }
                default:break;
            }
        //}
    }

    /**
     * The
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

}

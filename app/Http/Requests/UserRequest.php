<?php

namespace App\Http\Requests;

use App\Http\Requests\Request; 
 

class UserRequest  extends Request {

    /**
     * The product validation rules.
     *
     * @return array
     */
    public function rules() { 
            switch ( $this->method() ) {

                case 'GET':
                case 'DELETE': {
                        return [ ];
                    }
                case 'POST': {
                        return [
                            'username'             => 'required',
                            'mobile'             => 'required|unique:users',
                            'email'             => 'required|unique:users|email'
                        ];
                    }
                case 'PUT':
                case 'PATCH': {

                    if ( $user = $this->user ) {

                        return [
                            'username'             => 'required' ,
                            'email'             => 'required|email|unique:users,email,'.$user->id ,
                            'mobile'             => 'required|unique:users,mobile,'.$user->id
                            ];
                    }
                }
                default:break;
            }
        //}
    }

    /**
     * The
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

}

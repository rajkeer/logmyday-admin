<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MyProject extends BaseModel {
    /*
      |--------------------------------------------------------------------------
      | Client Model
      |--------------------------------------------------------------------------
      | Author : Naru Lal keer
      | This Model is used for pages table related operations.
      |
     */

    protected $table = 'user_project_relations';
    protected $primaryKey = 'id';
    protected $hidden = ['created_at', 'updated_at'];
    protected $guarded = ['created_at', 'updated_at', 'id'];
    
    public function user() {
        return $this->belongsTo('App\Models\user', 'user_id', 'id');
    }
    public function project() {
        return $this->belongsTo('App\Models\Project', 'project_id', 'id');
    }
}

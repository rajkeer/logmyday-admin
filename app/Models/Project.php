<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Project extends BaseModel {
    /*
      |--------------------------------------------------------------------------
      | Project Model
      |--------------------------------------------------------------------------
      | Author : Naru Lal keer
      | This Model is used for pages table related operations.
      |
     */

    protected $table = 'projects';
    protected $primaryKey = 'id';
    protected $hidden = ['created_at', 'updated_at'];
    protected $guarded = ['created_at', 'updated_at', 'id'];

    public function branch() {
        return $this->belongsTo('App\Models\Branch', 'company_branch_id', 'id');
    }

    public function client() {
        return $this->hasOne('App\Models\Client', 'id','client_id');
    }

}

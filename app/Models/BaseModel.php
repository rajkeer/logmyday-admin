<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model {
    /*
      |--------------------------------------------------------------------------
      | BASE Model
      |--------------------------------------------------------------------------
      | Author : Naru Lal keer
      | BASE MODEL FOR ALL SUB MODELS
      |
     */
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Company extends BaseModel {
    /*
      |--------------------------------------------------------------------------
      | Company Model
      |--------------------------------------------------------------------------
      | Author : Naru Lal keer
      | This Model is used for pages table related operations.
      |
     */

    protected $table = 'companies';
    protected $primaryKey = 'id';
    protected $hidden = ['created_at', 'updated_at'];
    protected $guarded = ['created_at', 'updated_at', 'id'];

}

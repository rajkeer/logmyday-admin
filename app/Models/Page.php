<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Page extends BaseModel {
    /*
      |--------------------------------------------------------------------------
      | Page Model
      |--------------------------------------------------------------------------
      | Author : Naru Lal keer
      | This Model is used for pages table related operations.
      |
     */

    protected $table = 'pages';
    protected $primaryKey = 'id';
    protected $hidden = ['created_at', 'updated_at'];
    protected $guarded = ['created_at', 'updated_at', 'id'];

}

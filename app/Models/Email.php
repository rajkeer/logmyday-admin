<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Email extends BaseModel {
    /*
      |--------------------------------------------------------------------------
      | Client Model
      |--------------------------------------------------------------------------
      | Author : Naru Lal keer
      | This Model is used for emails table related operations.
      |
     */

    protected $table = 'emails';
    protected $primaryKey = 'id';
    protected $hidden = ['created_at', 'updated_at'];
    protected $guarded = ['created_at', 'updated_at', 'id'];

}

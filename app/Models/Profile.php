<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Profile extends BaseModel {
    /*
      |--------------------------------------------------------------------------
      | Project Model
      |--------------------------------------------------------------------------
      | Author : Naru Lal keer
      | This Model is used for profiles table related operations.
      |
     */

    protected $table = 'profiles';
    protected $primaryKey = 'id';
    protected $hidden = ['created_at', 'updated_at'];
    protected $guarded = ['created_at', 'updated_at', 'id'];

    public function user() {
        return $this->belongsTo('App\Models\Branch', 'company_branch_id', 'id');
    }

    public function address() {
        return $this->hasOne('App\Models\Address', 'id', 'primary_address_id');
    }

    public function email() {
        return $this->hasOne('App\Models\Email', 'id', 'primary_email_id');
    }

    public function emails() {
        return $this->hasMany('App\Models\Email', 'user_id', 'user_id');
    }

    public function phones() {
        return $this->hasMany('App\Models\Phone', 'user_id', 'user_id');
    }

    public function projects() {
        return $this->hasMany('App\Models\MyProject', 'user_id', 'user_id')->with('project');
    }

}

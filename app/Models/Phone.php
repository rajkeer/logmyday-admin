<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Phone extends BaseModel {
    /*
      |--------------------------------------------------------------------------
      | Client Model
      |--------------------------------------------------------------------------
      | Author : Naru Lal keer
      | This Model is used for contacts table related operations.
      |
     */

    protected $table = 'contacts';
    protected $primaryKey = 'id';
    protected $hidden = ['created_at', 'updated_at'];
    protected $guarded = ['created_at', 'updated_at', 'id'];


}

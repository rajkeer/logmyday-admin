<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User extends BaseModel {
    /*
      |--------------------------------------------------------------------------
      | User Model
      |--------------------------------------------------------------------------
      | Author : Naru Lal keer
      | This Model is used for pages table related operations.
      |
     */

    protected $table = 'users';
    protected $primaryKey = 'id';
    protected $hidden = ['created_at', 'updated_at'];
    protected $guarded = ['created_at', 'updated_at', 'id'];

    public function projects() {
        return $this->hasMany('App\Models\MyProject', 'user_id', 'user_id')->with('project');
    }

}

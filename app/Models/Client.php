<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Client extends BaseModel {
    /*
      |--------------------------------------------------------------------------
      | Client Model
      |--------------------------------------------------------------------------
      | Author : Naru Lal keer
      | This Model is used for pages table related operations.
      |
     */

    protected $table = 'clients';
    protected $primaryKey = 'id';
    protected $hidden = ['created_at', 'updated_at'];
    protected $guarded = ['created_at', 'updated_at', 'id'];

    public function company() {
        return $this->belongsTo('App\Models\Company', 'company_id', 'id');
    }

}

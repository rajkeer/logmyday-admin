@extends($theme_admin_layout)

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading clearfix">
                <h4 class="panel-title">{{$heading_title}}</h4>
            </div>
            <div class="panel-body">
                {!! Form::model($company, ['route' => ['companies.store'],'class'=>'form-horizontal user-form','id'=>'user-form','enctype'=>'multipart/form-data']) !!}

                @include('admin.companies.form')

                {!! Form::close() !!} 

            </div>
        </div>
    </div>
</div>

@endsection
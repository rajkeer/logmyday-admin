<div class="form-body">
    <div class="alert alert-danger display-hide">
        <button class="close" data-close="alert"></button> Please fill the required field! </div>

    <div class="form-group {{ $errors->first('name', ' has-error') }} @if(session('field_errors')) {{ 'has-error' }} @endif">
        <label class="control-label col-md-3">Name <span class="required"> * </span></label>
        <div class="col-md-8"> 
            {!! Form::text('name',null, ['class' => 'form-control','data-required'=>1,'required'])  !!} 
            <span class="help-block">{{ $errors->first('name', ':message') }}  
        </div>
    </div>  


    <div class="form-group {{ $errors->first('tag_line', ' has-error') }} @if(session('field_errors')) {{ 'has-error' }} @endif">
        <label class="control-label col-md-3">Tag Line <span class="required"> * </span></label>
        <div class="col-md-8"> 
            {!! Form::text('tag_line',null, ['class' => 'form-control','data-required'=>1,'required'])  !!} 
            <span class="help-block">{{ $errors->first('tag_line', ':message') }}  
        </div>
    </div>  

    <div class="form-group {{ $errors->first('status', ' has-error') }} @if(session('field_errors')) {{ 'has-error' }} @endif">
        <label class="control-label col-md-3">Status <span class="required"> * </span></label>
        <div class="col-md-8"> 
            {!! Form::select('status',['1'=>'Active','2'=>'Trash','0'=>'In Active'],null,['class' => 'form-control','data-required'=>1,'required'])  !!} 
            <span class="help-block">{{ $errors->first('status', ':message') }}  
        </div>
    </div>  
</div>
<div class="form-actions">
    <div class="row">
        <div class="col-md-offset-3 col-md-9">
            {!! Form::submit(' Save ', ['class'=>'btn  btn-primary text-white','id'=>'saveBtn']) !!} 

            <a href="{{route('companies.index')}}">
                {!! Form::button('Back', ['class'=>'btn btn-warning text-white']) !!} </a>
        </div>
    </div>
</div>

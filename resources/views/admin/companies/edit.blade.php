
@extends($theme_admin_layout)

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading clearfix">
                <h4 class="panel-title">{{$heading_title}}</h4>
            </div>
            <div class="panel-body">
                {!! Form::model($company, ['method' => 'PATCH', 'route' => ['companies.update', $company->id],'class'=>'form-horizontal user-form','id'=>'form_sample_3','enctype'=>'multipart/form-data']) !!}         

                @include('admin.companies.form')

                {!! Form::close() !!} 

            </div>
        </div>
    </div>
</div>

@endsection


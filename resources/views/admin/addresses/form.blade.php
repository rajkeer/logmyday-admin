<div class="form-body-address">
    {!! Form::hidden('address[id]',isset($address->id)?$address->id:null)  !!} 
    <div class="form-group {{ $errors->first('address[name]', ' has-error') }} @if(session('field_errors')) {{ 'has-error' }} @endif">
        <label class="control-label col-md-3">Address <span class="required"> * </span></label>
        <div class="col-md-8"> 
            {!! Form::text('address[name]',isset($address->name)?$address->name:null, ['class' => 'form-control','data-required'=>1,'required'])  !!} 
            <span class="help-block">{{ $errors->first('address[name]', ':message') }}  
        </div>
    </div>  
    <div class="form-group {{ $errors->first('address[additional_details]', ' has-error') }} @if(session('field_errors')) {{ 'has-error' }} @endif">
        <label class="control-label col-md-3">Address Line 2</label>
        <div class="col-md-8"> 
            {!! Form::text('address[additional_details]',isset($address->additional_details)?$address->additional_details:null, ['class' => 'form-control'])  !!} 
            <span class="help-block">{{ $errors->first('address[additional_details]', ':message') }}  
        </div>
    </div> 

    <div class="form-group {{ $errors->first('address[house_number]', ' has-error') }} @if(session('field_errors')) {{ 'has-error' }} @endif">
        <label class="control-label col-md-3">House number <span class="required"> * </span></label>
        <div class="col-md-8"> 
            {!! Form::text('address[house_number]',isset($address->house_number)?$address->house_number:null, ['class' => 'form-control','data-required'=>1,'required'])  !!} 
            <span class="help-block">{{ $errors->first('address[house_number]', ':message') }}  
        </div>
    </div>  
    <div class="form-group {{ $errors->first('address[society]', ' has-error') }} @if(session('field_errors')) {{ 'has-error' }} @endif">
        <label class="control-label col-md-3">Society<span class="required"> * </span></label>
        <div class="col-md-8"> 
            {!! Form::text('address[society]',isset($address->society)?$address->society:null, ['class' => 'form-control','data-required'=>1,'required'])  !!} 
            <span class="help-block">{{ $errors->first('address[society]', ':message') }}  
        </div>
    </div> 


    <div class="form-group {{ $errors->first('address[city]', ' has-error') }} @if(session('field_errors')) {{ 'has-error' }} @endif">
        <label class="control-label col-md-3">City<span class="required"> * </span></label>
        <div class="col-md-8"> 
            {!! Form::text('address[city]',isset($address->city)?$address->city:null, ['class' => 'form-control','data-required'=>1,'required'])  !!} 
            <span class="help-block">{{ $errors->first('address[city]', ':message') }}  
        </div>
    </div> 
    <div class="form-group {{ $errors->first('address[state]', ' has-error') }} @if(session('field_errors')) {{ 'has-error' }} @endif">
        <label class="control-label col-md-3">State<span class="required"> * </span></label>
        <div class="col-md-8"> 
            {!! Form::text('address[state]',isset($address->state)?$address->state:null, ['class' => 'form-control','data-required'=>1,'required'])  !!} 
            <span class="help-block">{{ $errors->first('address[state]', ':message') }}  
        </div>
    </div> 
    <div class="form-group {{ $errors->first('address[country]', ' has-error') }} @if(session('field_errors')) {{ 'has-error' }} @endif">
        <label class="control-label col-md-3">Country <span class="required"> * </span></label>
        <div class="col-md-8"> 
            {!! Form::select('address[country]',['india'=>'India','usa'=>'USA'],isset($address->country)?$address->country:null,['class'=>'form-control'])  !!} 
            <span class="help-block">{{ $errors->first('country', ':message') }}  
        </div>
    </div> 
    <div class="form-group {{ $errors->first('address[pincode]', ' has-error') }} @if(session('field_errors')) {{ 'has-error' }} @endif">
        <label class="control-label col-md-3">Pincode<span class="required"> * </span></label>
        <div class="col-md-8"> 
            {!! Form::text('address[pincode]',isset($address->pincode)?$address->pincode:null, ['class' => 'form-control','data-required'=>1,'required'])  !!} 
            <span class="help-block">{{ $errors->first('address[pincode]', ':message') }}  
        </div>
    </div> 

</div> 




</div>


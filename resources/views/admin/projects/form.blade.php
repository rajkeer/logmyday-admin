<div class="form-body">
    <div class="alert alert-danger display-hide">
        <button class="close" data-close="alert"></button> Please fill the required field! </div>

    <div class="form-group {{ $errors->first('company_id', ' has-error') }} @if(session('field_errors')) {{ 'has-error' }} @endif">
        <label class="control-label col-md-3">Company <span class="required"> * </span></label>
        <div class="col-md-8">
            {!! Form::select('company_id',$companies,$company_id,['class'=>'form-control'])  !!}
            <span class="help-block">{{ $errors->first('company_id', ':message') }}
        </div>
    </div>
    <div class="form-group {{ $errors->first('company_branch_id', ' has-error') }} @if(session('field_errors')) {{ 'has-error' }} @endif">
        <label class="control-label col-md-3">Branch Name <span class="required"> * </span></label>
        <div class="col-md-8">
            {!! Form::select('company_branch_id',[],null,['class'=>'form-control'])  !!}
            <span class="help-block">{{ $errors->first('company_branch_id', ':message') }}
        </div>
    </div>
    <div class="form-group {{ $errors->first('client_id', ' has-error') }} @if(session('field_errors')) {{ 'has-error' }} @endif">
        <label class="control-label col-md-3">Client <span class="required"> * </span></label>
        <div class="col-md-8">
            {!! Form::select('client_id',[],null,['class'=>'form-control'])  !!}
            <span class="help-block">{{ $errors->first('client_id', ':message') }}
        </div>
    </div>
    <div class="form-group {{ $errors->first('name', ' has-error') }} @if(session('field_errors')) {{ 'has-error' }} @endif">
        <label class="control-label col-md-3">Project Name <span class="required"> * </span></label>
        <div class="col-md-8">
            {!! Form::text('name',null, ['class' => 'form-control','data-required'=>1,'required'])  !!}
            <span class="help-block">{{ $errors->first('name', ':message') }}
        </div>
    </div>
    <div class="form-group {{ $errors->first('technology', ' has-error') }} @if(session('field_errors')) {{ 'has-error' }} @endif">
        <label class="control-label col-md-3">Technology <span class="required"> * </span></label>
        <div class="col-md-8">
            {!! Form::text('technology',null, ['class' => 'form-control','data-required'=>1,'required'])  !!}
            <span class="help-block">{{ $errors->first('technology', ':message') }}
        </div>
    </div>
    <div class="form-group {{ $errors->first('description', ' has-error') }}">
        <label class="control-label col-md-3">Description<span class="required"> </span></label>
        <div class="col-md-8">
            {!! Form::textarea('description',null, ['class' => 'form-control ckeditor form-cascade-control','data-required'=>1,'rows'=>3,'cols'=>5])  !!}
            <span class="help-block">{{ $errors->first('description', ':message') }}</span>
        </div>
    </div>
    <div class="form-group {{ $errors->first('status', ' has-error') }} @if(session('field_errors')) {{ 'has-error' }} @endif">
        <label class="control-label col-md-3">Project Status <span class="required"> * </span></label>
        <div class="col-md-8">
            {!! Form::select('status',['1'=>'Active','2'=>'Trash','0'=>'In Active'],null,['class' => 'form-control','data-required'=>1,'required'])  !!}
            <span class="help-block">{{ $errors->first('status', ':message') }}
        </div>
    </div>
</div>
<div class="form-actions">
    <div class="row">
        <div class="col-md-offset-3 col-md-9">
            {!! Form::submit(' Save ', ['class'=>'btn  btn-primary text-white','id'=>'saveBtn']) !!}

            <a href="{{route('projects.index')}}">
                {!! Form::button('Back', ['class'=>'btn btn-warning text-white']) !!} </a>
        </div>
    </div>
</div>
@section('footer')
<script>
    $(document).ready(function () {
        var branch_id = '{{$project["company_branch_id"]}}';
        var client_id = '{{$project["client_id"]}}';

        $('select[name="company_id"]').on('change', function () {
            var countryId = $(this).val();
            if (countryId) {
                $.ajax({
                    url: '/admin/getbranches/' + countryId,
                    type: "GET",
                    dataType: "json",
                    beforeSend: function () {
                        $('#loader').css("visibility", "visible");
                    },
                    success: function (data) {

                        $('select[name="company_branch_id"]').empty();

                        $.each(data, function (key, value) {

                            $('select[name="company_branch_id"]').append('<option value="' + key + '">' + value + '</option>');

                        });
                        $('select[name="company_branch_id"]').val(branch_id).trigger('change');
                    },
                    complete: function () {
                        $('#loader').css("visibility", "hidden");
                    }
                });
                
                $.ajax({
                    url: '/admin/getclients/' + countryId,
                    type: "GET",
                    dataType: "json",
                    beforeSend: function () {
                        $('#loader').css("visibility", "visible");
                    },
                    success: function (data) {

                        $('select[name="client_id"]').empty();

                        $.each(data, function (key, value) {

                            $('select[name="client_id"]').append('<option value="' + key + '">' + value + '</option>');

                        });
                        $('select[name="client_id"]').val(client_id).trigger('change');
                    },
                    complete: function () {
                        $('#loader').css("visibility", "hidden");
                    }
                });                
            } else {
                $('select[name="company_branch_id"]').empty();
                $('select[name="client_id"]').empty();
            }

        }).trigger('change');

    });
</script>
@endsection
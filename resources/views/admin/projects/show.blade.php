@extends($theme_admin_layout)

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading clearfix">
                <h4 class="panel-title">{{$heading_title}}</h4>
            </div>
            <div class="panel-body">
                <div class="detail show-detiai col-sm-12">
                    <div class="form-group col-sm-12">
                        <label class="control-label col-md-4">Name</label>
                        <div class="col-md-8"> 
                            {{$project->name}}
                        </div>
                    </div> 
                    <div class="form-group col-sm-12">
                        <label class="control-label col-md-4">Description</label>
                        <div class="col-md-8"> 
                            {{$project->description}}
                        </div>
                    </div>  
                    <div class="form-group col-sm-12">
                        <label class="control-label col-md-4">Technology</label>
                        <div class="col-md-8"> 
                            {{$project->technology}}
                        </div>
                    </div>                    
                    <div class="form-group col-sm-12">
                        <label class="control-label col-md-4">Status</label>
                        <div class="col-md-8"> 
                            @if($project->status==1)
                            <span class="label label-success">Active</span>
                            @elseif($project->status==2)
                            <span class="label label-danger">Trash</span>
                            @elseif($project->status==0)
                            <span class="label label-info">In Active</span>
                            @endif
                        </div>
                    </div> 
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <a href="{{route('projects.index')}}">
                                    {!! Form::button('Back', ['class'=>'btn btn-warning text-white']) !!} </a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@extends($theme_admin_layout)

@section('content')
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading">
                <h4 class="panel-title hidden">{{$heading_title}}</h4>
                <div class="row">
                    <form action="{{route('users.index')}}" method="get" id="filter_data">
                        <div class="col-md-2">
                             {!! Form::select('status',[''=>'--Any Status--','1'=>'Active','2'=>'Trash','0'=>'In Active'],$status,['class'=>'form-control'])  !!}        
                        </div>
                        <div class="col-md-2"> 
                            {!! Form::select('user_type',$user_types,$user_type,['class'=>'form-control'])  !!} 
                        </div>
                       <div class="col-md-2">
                            <input value="{{ (isset($_REQUEST['search']))?$_REQUEST['search']:''}}" placeholder="Type here ..." type="text" name="search" id="search" class="form-control" >
                        </div>
                        <div class="col-md-2">
                            <input type="submit" value="Search" class="btn btn-primary form-control">
                        </div>

                    </form>
                    <div class="col-md-2">
                        <a href="{{ route('users.index') }}">   <input type="submit" value="Reset" class="btn btn-default form-control"> </a>
                    </div>
                    <div class="col-md-2 pull-right">
                        <div style="width: 150px;" class="input-group"> 
                            <a href="{{ route('users.create')}}">
                                <button class="btn  btn-primary"><i class="fa fa-user-plus"></i>{{$button_add}}</button> 
                            </a>
                        </div>
                    </div> 
                </div>
            </div>


            @if(Session::has('flash_alert_notice'))
            <div class="alert alert-success alert-dismissable" style="margin:10px">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <i class="icon fa fa-check"></i>  
                {{ Session::get('flash_alert_notice') }} 
            </div>
            @endif
            <div class="panel-body">
                <div class="table-responsive project-stats">  
                    <table class="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>User Name</th>
                                <th>Role</th>
                                <th>Email</th>
                                <th>Mobile</th>
                                <th>Created At</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($results as $result)
                            <tr>
                                <th scope="row">{{$result->id}}</th>
                                <td>{{$result->username}}</td>
                                <td>
                                    @if(isset($user_types[$result->user_type]))
                                    {{$user_types[$result->user_type]}}
                                    @else
                                    -
                                    @endif
                                </td>
                                <td>{{$result->email}}</td>
                                <td>{{$result->mobile}}</td>
                                <td>{{$result->created_at}}</td>
                                <td>
                                    @if($result->status==1)
                                    <span class="label label-success">Active</span>
                                    @elseif($result->status==2)
                                    <span class="label label-danger">Trash</span>
                                    @elseif($result->status==0)
                                    <span class="label label-info">In Active</span>
                                    @endif</td>
                                <td>
                                    <div class="btn-group">
                                        <a href="{{ route('users.show',$result->id)}}">
                                            <i class="fa fa-fw fa-eye" title="view"></i> 
                                        </a>

                                        <a href="{{ route('users.edit',$result->id)}}">
                                            <i class="fa fa-fw fa-pencil-square-o" title="edit"></i> 
                                        </a>

                                        {!! Form::open(array('class' => 'form-inline pull-rights deletion-form', 'method' => 'DELETE',  'id'=>'deleteForm_'.$result->id, 'route' => array('users.destroy', $result->id))) !!}
                                        <button class='delbtn btn btn-danger btn-xs' type="submit" name="remove_levels" value="delete" id="{{$result->id}}"><i class="fa fa-fw fa-trash" title="Delete"></i></button>

                                        {!! Form::close() !!}
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="center" align="center">  {!! $results->render() !!}</div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
<div class="form-body">
    <div class="alert alert-danger display-hide">
        <button class="close" data-close="alert"></button> Please fill the required field! </div>

    <div class="form-group {{ $errors->first('username', ' has-error') }} @if(session('field_errors')) {{ 'has-error' }} @endif">
        <label class="control-label col-md-3">User Name <span class="required"> * </span></label>
        <div class="col-md-8"> 
            {!! Form::text('username',null, ['class' => 'form-control','data-required'=>1,'required'])  !!} 
            <span class="help-block">{{ $errors->first('username', ':message') }}  
        </div>
    </div>  
    <div class="form-group {{ $errors->first('user_type', ' has-error') }} @if(session('field_errors')) {{ 'has-error' }} @endif">
        <label class="control-label col-md-3">Types <span class="required"> * </span></label>
        <div class="col-md-8"> 
            {!! Form::select('user_type',$user_types,null,['class'=>'form-control'])  !!} 
            <span class="help-block">{{ $errors->first('user_type', ':message') }}  
        </div>
    </div> 
    <div class="form-group {{ $errors->first('email', ' has-error') }} @if(session('field_errors')) {{ 'has-error' }} @endif">
        <label class="control-label col-md-3">Email <span class="required"> * </span></label>
        <div class="col-md-8"> 
            {!! Form::text('email',null, ['class' => 'form-control','data-required'=>1,'required'])  !!} 
            <span class="help-block">{{ $errors->first('email', ':message') }}  
        </div>
    </div>  
    <div class="form-group {{ $errors->first('mobile', ' has-error') }} @if(session('field_errors')) {{ 'has-error' }} @endif">
        <label class="control-label col-md-3">Mobile <span class="required"> * </span></label>
        <div class="col-md-8"> 
            {!! Form::text('mobile',null, ['class' => 'form-control','data-required'=>1,'required'])  !!} 
            <span class="help-block">{{ $errors->first('mobile', ':message') }}  
        </div>
    </div>  

    <div class="form-group {{ $errors->first('password', ' has-error') }} @if(session('field_errors')) {{ 'has-error' }} @endif">
        <label class="control-label col-md-3">Password</label>
        <div class="col-md-8"> 
            {!! Form::text('new_password','', ['class' => 'form-control'])  !!} 
            <span class="help-block">{{ $errors->first('password', ':message') }}  
        </div>
    </div>  
    <div class="form-group {{ $errors->first('status', ' has-error') }} @if(session('field_errors')) {{ 'has-error' }} @endif">
        <label class="control-label col-md-3">Page slug <span class="required"> * </span></label>
        <div class="col-md-8"> 
            {!! Form::select('status',['1'=>'Active','2'=>'Trash','0'=>'In Active'],null,['class' => 'form-control','data-required'=>1,'required'])  !!} 
            <span class="help-block">{{ $errors->first('status', ':message') }}  
        </div>
    </div>  
</div>
<div class="form-actions">
    <div class="row">
        <div class="col-md-offset-3 col-md-9">
            {!! Form::submit(' Save ', ['class'=>'btn  btn-primary text-white','id'=>'saveBtn']) !!} 

            <a href="{{route('users.index')}}">
                {!! Form::button('Back', ['class'=>'btn btn-warning text-white']) !!} </a>
        </div>
    </div>
</div>

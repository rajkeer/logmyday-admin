<div class="form-body">
    <div class="alert alert-danger display-hide">
        <button class="close" data-close="alert"></button> Please fill the required field! </div>

    <div class="form-group {{ $errors->first('page_title', ' has-error') }} @if(session('field_errors')) {{ 'has-error' }} @endif">
        <label class="control-label col-md-3">Title <span class="required"> * </span></label>
        <div class="col-md-8"> 
            {!! Form::text('page_title',null, ['class' => 'form-control','data-required'=>1,'required'])  !!} 
            <span class="help-block">{{ $errors->first('page_title', ':message') }}  
        </div>
    </div>  

    <div class="form-group {{ $errors->first('page_description', ' has-error') }}">
        <label class="control-label col-md-3">Description<span class="required"> </span></label>
        <div class="col-md-8"> 
            {!! Form::textarea('page_description',null, ['class' => 'form-control ckeditor form-cascade-control','data-required'=>1,'rows'=>3,'cols'=>5])  !!}  
            <span class="help-block">{{ $errors->first('page_description', ':message') }}</span>
        </div>
    </div>

    <div class="form-group {{ $errors->first('page_slug', ' has-error') }} @if(session('field_errors')) {{ 'has-error' }} @endif">
        <label class="control-label col-md-3">Slug <span class="required"> * </span></label>
        <div class="col-md-8"> 
            {!! Form::text('page_slug',null, ['class' => 'form-control','data-required'=>1,'required'])  !!} 
            <span class="help-block">{{ $errors->first('page_slug', ':message') }}  
        </div>
    </div>  

    <div class="form-group {{ $errors->first('meta_title', ' has-error') }} @if(session('field_errors')) {{ 'has-error' }} @endif">
        <label class="control-label col-md-3">Meta Title <span class="required"> * </span></label>
        <div class="col-md-8"> 
            {!! Form::text('meta_title',null, ['class' => 'form-control','data-required'=>1,'required'])  !!} 
            <span class="help-block">{{ $errors->first('meta_title', ':message') }}  
        </div>
    </div>  

    <div class="form-group {{ $errors->first('meta_description', ' has-error') }}">
        <label class="control-label col-md-3"> Meta Description<span class="required"> </span></label>
        <div class="col-md-8"> 
            {!! Form::textarea('meta_description',null, ['class' => 'form-control ckeditor form-cascade-control','data-required'=>1,'rows'=>3,'cols'=>5])  !!}  
            <span class="help-block">{{ $errors->first('meta_description', ':message') }}</span>
        </div>
    </div>

    <div class="form-group {{ $errors->first('meta_keyword', ' has-error') }}">
        <label class="control-label col-md-3"> Meta keyword<span class="required"> </span></label>
        <div class="col-md-8"> 
            {!! Form::textarea('meta_keyword',null, ['class' => 'form-control ckeditor form-cascade-control','data-required'=>1,'rows'=>3,'cols'=>5])  !!}  
            <span class="help-block">{{ $errors->first('meta_keyword', ':message') }}</span>
        </div>
    </div>
    <div class="form-group {{ $errors->first('status', ' has-error') }} @if(session('field_errors')) {{ 'has-error' }} @endif">
        <label class="control-label col-md-3">Page slug <span class="required"> * </span></label>
        <div class="col-md-8"> 
            {!! Form::select('status',['1'=>'Active','2'=>'Trash','0'=>'In Active'],null,['class' => 'form-control','data-required'=>1,'required'])  !!} 
            <span class="help-block">{{ $errors->first('status', ':message') }}  
        </div>
    </div>  
</div>
<div class="form-actions">
    <div class="row">
        <div class="col-md-offset-3 col-md-9">
            {!! Form::submit(' Save ', ['class'=>'btn  btn-primary text-white','id'=>'saveBtn']) !!} 

            <a href="{{route('page')}}">
                {!! Form::button('Back', ['class'=>'btn btn-warning text-white']) !!} </a>
        </div>
    </div>
</div>


@extends($theme_admin_layout)

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading clearfix">
                <h4 class="panel-title">Edit Page</h4>
            </div>
            <div class="panel-body">
                {!! Form::model($page, ['method' => 'PATCH', 'route' => ['page.update', $page->id],'class'=>'form-horizontal user-form','id'=>'form_sample_3','enctype'=>'multipart/form-data']) !!}         

                @include('admin.pages.form')

                {!! Form::close() !!} 

            </div>
        </div>
    </div>
</div>

@endsection


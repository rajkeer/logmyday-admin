<div class="page-sidebar-wrapper">
    <!-- BEGIN SIDEBAR -->
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
        <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
        <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
        <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
        <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
        <ul class="page-sidebar-menu   " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
            <li class="nav-item {{ Request::path() == 'admin' ? 'active' : '' }}">
                <a href="{{route('admin')}}" class="nav-link ">
                    <i class="fa fa-2x fa-dashboard"></i>
                    <span class="title">Dashboard</span>
                    <span class="selected"></span>
                </a>
            </li>
            <li class="nav-item {{ Request::segment(2) == 'companies' ? 'active' : '' }}">
                <a href="{{route('companies.index')}}" class="nav-link ">
                    <i class="fa fa-2x fa-university"></i>
                    <span class="title">Company</span>
                    <span class="selected"></span>
                </a>
            </li>
            <li class="nav-item {{ Request::segment(2) == 'branches' ? 'active' : '' }}">
                <a href="{{route('branches.index')}}" class="nav-link ">
                    <i class="fa fa-2x fa-building-o"></i>
                    <span class="title">Branches</span>
                    <span class="selected"></span>
                </a>
            </li>
            <li class="nav-item {{ Request::segment(2) == 'users' ? 'active' : '' }}">
                <a href="{{route('users.index')}}" class="nav-link ">
                    <i class="fa fa-2x fa-slideshare"></i>
                    <span class="title">Users</span>
                    <span class="selected"></span>
                </a>
            </li>
            <li class="nav-item {{ Request::segment(2) == 'clients' ? 'active' : '' }}">
                <a href="{{route('clients.index')}}" class="nav-link ">
                    <i class="fa fa-2x fa-users"></i>
                    <span class="title">Clients</span>
                    <span class="selected"></span>
                </a>
            </li>
            <li class="nav-item {{ Request::segment(2) == 'projects' ? 'active' : '' }}">
                <a href="{{route('projects.index')}}" class="nav-link ">
                    <i class="fa fa-2x fa-cubes"></i>
                    <span class="title">Projects</span>
                    <span class="selected"></span>
                </a>
            </li>
            <li class="nav-item {{ Request::segment(2) == 'pages' ? 'active' : '' }}">
                <a href="{{route('page')}}" class="nav-link ">
                    <i class="fa fa-2x fa-file-text-o"></i>
                    <span class="title">Static Content</span>
                    <span class="selected"></span>
                </a>
            </li>
        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->
</div>
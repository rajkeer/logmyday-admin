<div class="page-header-inner ">
    <!-- BEGIN LOGO -->
    <div class="page-logo">
        <a href="{{route('admin')}}">
            <img src="{{asset('assets')}}/images/logo/logo.png" alt="logo" class="logo-default" width="200px;"/> </a>
        <!--<div class="menu-toggler sidebar-toggler">
             DOC: Remove the above "hide" to enable the sidebar toggler button on header 
        </div>-->
    </div>
    <!-- END LOGO -->
    <!-- BEGIN RESPONSIVE MENU TOGGLER -->
    <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
    <!-- END RESPONSIVE MENU TOGGLER -->

    <!-- BEGIN PAGE TOP -->
    <div class="page-top">
        <!-- BEGIN TOP NAVIGATION MENU -->
        <div class="page-actions">
            <div class="btn-group">
                <a href="{{route('companies.create')}}" class="btn red-haze btn-sm">
                    <i class="fa fa-plus-circle"></i>
                    <span class="hidden-sm hidden-xs">Add Company</span>
                </a>
            </div>
        </div>
        <div class="top-menu">

            <ul class="nav navbar-nav pull-right">
                <!-- END TODO DROPDOWN -->
                <!-- BEGIN USER LOGIN DROPDOWN -->
                <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                <li class="dropdown dropdown-user dropdown-dark">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                        <span class="username username-hide-on-mobile">     
                            @if(Auth::user() && Auth::user()->username)
                            {{Auth::user()->username}}
                            @else
                            User
                            @endif
                        </span>
                        <!-- DOC: Do not remove below empty space(&nbsp;) as its purposely used -->
                        <img alt="" class="img-circle" src="{{asset('assets')}}/layouts/layout4/img/avatar9.jpg" /> </a>
                    <ul class="dropdown-menu dropdown-menu-default">
                        <li>
                            <a href="{{route('profiles.index')}}">
                                <i class="icon-user"></i> My Profile </a>
                        </li>
                        <li>
                            <a href="app_todo_2.html">
                                <i class="icon-rocket"></i> My Tasks
                                <span class="badge badge-success"> 7 </span>
                            </a>
                        </li>
                        <li class="divider"> </li>
                        <li>
                            <a href="{{route('admin-logout')}}">
                                <i class="icon-key"></i> Log Out </a>
                        </li>
                    </ul>
                </li>
                <!-- END USER LOGIN DROPDOWN -->
            </ul>
        </div>
        <!-- END TOP NAVIGATION MENU -->
    </div>
    <!-- END PAGE TOP -->
</div>
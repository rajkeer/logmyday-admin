<div class="form-body">
    <div class="alert alert-danger display-hide">
        <button class="close" data-close="alert"></button> Please fill the required field! </div>


      <div class="form-group {{ $errors->first('company_id', ' has-error') }} @if(session('field_errors')) {{ 'has-error' }} @endif">
        <label class="control-label col-md-3">Company Name <span class="required"> * </span></label>
        <div class="col-md-8"> 
            {!! Form::select('company_id',$companies,null,['class'=>'form-control'])  !!} 
            <span class="help-block">{{ $errors->first('company_id', ':message') }}  
        </div>
    </div>  
    <div class="form-group {{ $errors->first('name', ' has-error') }} @if(session('field_errors')) {{ 'has-error' }} @endif">
        <label class="control-label col-md-3">Branch Name <span class="required"> * </span></label>
        <div class="col-md-8"> 
            {!! Form::text('name',null, ['class' => 'form-control','data-required'=>1,'required'])  !!} 
            <span class="help-block">{{ $errors->first('name', ':message') }}  
        </div>
    </div>  
    <hr/>

    <div class="form-group">
    @include('admin.addresses.form',array('address'=>$branch->address,'address_label'=>'Branch Address'))
    </div>
    <hr/>

    <div class="form-group {{ $errors->first('status', ' has-error') }} @if(session('field_errors')) {{ 'has-error' }} @endif">
        <label class="control-label col-md-3">Status <span class="required"> * </span></label>
        <div class="col-md-8"> 
            {!! Form::select('status',['1'=>'Active','2'=>'Trash','0'=>'In Active'],'',['class'=>'form-control'])  !!} 
            <span class="help-block">{{ $errors->first('status', ':message') }}  
        </div>
    </div>  
</div>
<div class="form-actions">
    <div class="row">
        <div class="col-md-offset-3 col-md-9">
            {!! Form::submit(' Save ', ['class'=>'btn  btn-primary text-white','id'=>'saveBtn']) !!} 

            <a href="{{route('branches.index')}}">
                {!! Form::button('Back', ['class'=>'btn btn-warning text-white']) !!} </a>
        </div>
    </div>
</div>

@extends($theme_admin_layout)

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading clearfix">
                <h4 class="panel-title">Add Company profile</h4>
            </div>
            <div class="panel-body">
                {!! Form::model($profile, ['route' => ['profiles.store'],'class'=>'form-horizontal user-form','id'=>'user-form','enctype'=>'multipart/form-data']) !!}

                @include('admin.profiles.form')

                {!! Form::close() !!} 

            </div>
        </div>
    </div>
</div>

@endsection
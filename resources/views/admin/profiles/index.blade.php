@extends($theme_admin_layout)

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-white">
            <div class="panel-heading clearfix">
                <h4 class="panel-title">{{$heading_title}}|{{$user->username}}<a href="{{route('profiles.edit',$profile->id)}}" class="pull-right"><i class="fa fa-pencil"></i>Edit Profile</a></h4>
            </div>
            <div class="panel-body">
                <div class="detail show-detiai col-sm-12">
                    <div class="form-group col-sm-12">
                        <label class="control-label col-md-4">Name</label>
                        <div class="col-md-8">
                            {{$profile->first_name}}
                        </div>
                    </div>
                    <div class="form-group col-sm-12">
                        <label class="control-label col-md-4">Last Name</label>
                        <div class="col-md-8">
                            {{$profile->last_name}}
                        </div>
                    </div>
                    <div class="form-group col-sm-12">
                        <label class="control-label col-md-4">Date Of Birth</label>
                        <div class="col-md-8">
                            {{$profile->date_of_birth}}
                        </div>
                    </div>
                    <div class="form-group col-sm-12">
                        <label class="control-label col-md-4">Status</label>
                        <div class="col-md-8">
                            @if($profile->status==1)
                            <span class="label label-success">Active</span>
                            @elseif($profile->status==2)
                            <span class="label label-danger">Trash</span>
                            @elseif($profile->status==0)
                            <span class="label label-info">In Active</span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group col-sm-12">
                        <h4 class="control-label col-md-12">Contact Details</h4>

                        <div class="row">
                            <div class="form-group col-sm-12">
                                <label class="control-label col-md-4">Emails</label>
                                <div class="col-md-8">
                                    @if($profile->emails && $profile->emails->count())
                                    <ul class="email-list">
                                        @foreach($profile->emails as $email)
                                        <li><i class="fa fa-envelope"></i>
                                            <a href="mailto:{{$email->email}}">{{$email->email}}</a>
                                            @if($email->id ==$profile->primary_email_id)
                                            <span class="primary danger">(Primary)</span>
                                            @endif
                                        </li>
                                        @endforeach
                                    </ul>
                                    @endif

                                </div>
                            </div>
                            <div class="form-group col-sm-12">
                                <label class="control-label col-md-4">Phone Numbers</label>
                                <div class="col-md-8">
                                    @if($profile->phones && $profile->phones->count())
                                    <ul class="phone-list">
                                        @foreach($profile->phones as $phone)
                                        <li><i class="fa fa-phone"></i>
                                            <a href="tel:{{$phone->contact}}">{{$phone->contact}}</a>
                                            @if($phone->id ==$profile->primary_contact_id)
                                            <span class="primary danger">(Primary)</span>
                                            @endif
                                        </li>
                                        @endforeach
                                    </ul>
                                    @endif

                                </div>
                            </div>                            
                            <div class="form-group col-sm-12">
                                <label class="control-label col-md-4">Address</label>
                                <div class="col-md-8">
                                    {{$profile->address->house_number}}
                                    {{$profile->address->society}}
                                    {{$profile->address->city}}
                                    {{$profile->address->state}},
                                    {{$profile->address->country}}-
                                    {{$profile->address->pincode}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-sm-12">
                        <h4 class="control-label col-md-12">Project Details</h4>

                        <div class="row">
                            <div class="form-group col-sm-12">
                                <label class="control-label col-md-4"></label>
                                <div class="col-md-8">
                                    @if($profile->projects && $profile->projects->count())
                                    <ul class="email-list">
                                        @foreach($profile->projects as $project)
                                        <li>
                                            {{$project->project->name}}|{{$project->project->technology}}
                                            @if($project->id ==$profile->primary_user_project_relation_id)
                                            <span class="primary danger">(Primary)</span>
                                            @endif
                                        </li>
                                        @endforeach
                                    </ul>
                                    @else 
                                    No Project Assigned Yet
                                    @endif

                                </div>
                            </div>     
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
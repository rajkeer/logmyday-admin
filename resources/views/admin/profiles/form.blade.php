<div class="form-body">
    <div class="alert alert-danger display-hide">
        <button class="close" data-close="alert"></button> Please fill the required field! </div>


    <div class="form-group {{ $errors->first('name', ' has-error') }} @if(session('field_errors')) {{ 'has-error' }} @endif">
        <label class="control-label col-md-3">First Name <span class="required"> * </span></label>
        <div class="col-md-8"> 
            {!! Form::text('first_name',null, ['class' => 'form-control','data-required'=>1,'required'])  !!} 
            <span class="help-block">{{ $errors->first('first_name',':message') }}  
        </div>
    </div>  
    <div class="form-group {{ $errors->first('last_name', ' has-error') }} @if(session('field_errors')) {{ 'has-error' }} @endif">
        <label class="control-label col-md-3">Last Name <span class="required"> * </span></label>
        <div class="col-md-8"> 
            {!! Form::text('last_name',null, ['class' => 'form-control','data-required'=>1,'required'])  !!} 
            <span class="help-block">{{ $errors->first('last_name', ':message') }}  
        </div>
    </div>    
        <div class="form-group {{ $errors->first('date_of_birth', ' has-error') }} @if(session('field_errors')) {{ 'has-error' }} @endif">
        <label class="control-label col-md-3">Date Of Birth <span class="required"> * </span></label>
        <div class="col-md-8"> 
            {!! Form::text('date_of_birth',null, ['class' => 'form-control','data-required'=>1,'required'])  !!} 
            <span class="help-block">{{ $errors->first('date_of_birth', ':message') }}  
        </div>
    </div> 
    
    <hr/>

    <div class="form-group">
    @include('admin.addresses.form',array('address'=>$profile->address,'address_label'=>'profile Address'))
    </div>
    <hr/>

    <div class="form-group {{ $errors->first('status', ' has-error') }} @if(session('field_errors')) {{ 'has-error' }} @endif">
        <label class="control-label col-md-3">Status <span class="required"> * </span></label>
        <div class="col-md-8"> 
            {!! Form::select('status',['1'=>'Active','2'=>'Trash','0'=>'In Active'],'',['class'=>'form-control'])  !!} 
            <span class="help-block">{{ $errors->first('status', ':message') }}  
        </div>
    </div>  
</div>
<div class="form-actions">
    <div class="row">
        <div class="col-md-offset-3 col-md-9">
            {!! Form::submit(' Save ', ['class'=>'btn  btn-primary text-white','id'=>'saveBtn']) !!} 

            <a href="{{route('profiles.index')}}">
                {!! Form::button('Back', ['class'=>'btn btn-warning text-white']) !!} </a>
        </div>
    </div>
</div>


<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Log My Day</title>
        <meta name="viewport" content="Log My Day">
        <meta name="description" content="Log My Day" />
        <meta name="keywords" content="Log My Day" />
        <meta name="author" content="Log My Day" />



        <!-- Facebook and Twitter integration -->
        <meta property="og:title" content=""/>
        <meta property="og:image" content=""/>
        <meta property="og:url" content=""/>
        <meta property="og:site_name" content=""/>
        <meta property="og:description" content=""/>
        <meta name="twitter:title" content="" />
        <meta name="twitter:image" content="" />
        <meta name="twitter:url" content="" />
        <meta name="twitter:card" content="" />

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <link rel="shortcut icon" href="{{asset('assets')}}/images/logo/favicon.png">

        <!-- <link href='https://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic,700italic' rel='stylesheet' type='text/css'> -->

        <!-- Animate.css -->
        <link rel="stylesheet" href="{{asset('assets/website')}}/css/animate.css">
        <!-- Icomoon Icon Fonts-->
        <link rel="stylesheet" href="{{asset('assets/website')}}/css/icomoon.css">
        <!-- Simple Line Icons -->
        <link rel="stylesheet" href="{{asset('assets/website')}}/css/simple-line-icons.css">
        <!-- Bootstrap  -->
        <link rel="stylesheet" href="{{asset('assets/website')}}/css/bootstrap.css">
        <!-- Owl Carousel  -->
        <link rel="stylesheet" href="{{asset('assets/website')}}/css/owl.carousel.min.css">
        <link rel="stylesheet" href="{{asset('assets/website')}}/css/owl.theme.default.min.css">
        <!-- Style -->
        <link rel="stylesheet" href="{{asset('assets/website')}}/css/style.css">


        <!-- Modernizr JS -->
        <script src="{{asset('assets/website')}}/js/modernizr-2.6.2.min.js"></script>
        <!-- FOR IE9 below -->
        <!--[if lt IE 9]>
        <script src="{{asset('assets/website')}}/js/respond.min.js"></script>
        <![endif]-->

    </head>
    <body>
        <header role="banner" id="fh5co-header">
            <div class="fluid-container">
                <nav class="navbar navbar-default">
                    <div class="navbar-header">
                        <!-- Mobile Toggle Menu Button -->
                        <a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"><i></i></a>
                        <a class="" href="{{route('home')}}">
                            <img src="{{asset('assets')}}/images/logo/logo.png" alt="logo" class="logo-default" width="200px;"/> </a>
                        </a> 
                    </div>
                    <div id="navbar" class="navbar-collapse collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li class="active"><a href="#" data-nav-section="home"><span>Home</span></a></li>
                            <li><a href="#" data-nav-section="services"><span>Services</span></a></li>
                            <li><a href="#" data-nav-section="testimony"><span>Testimony</span></a></li>
                            <li><a href="#" data-nav-section="team"><span>Team</span></a></li>
                            <li class="call-to-action"><a class="log-in" href="{{route('login')}}"><span>Login</span></a></li>
                        </ul>
                    </div>
                </nav>
            </div>
        </header>

        @yield('content')

        <div id="fh5co-footer" role="contentinfo" style="padding-bottom: 0px;">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 to-animate">
                        <h3 class="section-title">About Us</h3>
                        <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics.</p>
                        <p class="copy-right">&copy; <?php echo date('Y'); ?> LogMyDay Pvt. Ltd. <br>All Rights Reserved. <br/>
                            Designed by <a href="http://www.infowaynic.com/" target="_blank">LogMyDay Team.</a>
                        </p>
                    </div>

                    <div class="col-md-4 to-animate">
                        <h3 class="section-title">Our Address</h3>
                        <ul class="contact-info">
                            <li><i class="icon-map2"></i>1<sup>st</sup> Floor, Speciality Business Center,<br/> Balewadi, Pune - 411045,<br/> Maharashtra, India.</li>
                            <li><i class="icon-phone"></i>+91 808 708 5108</li>
                            <li><i class="icon-envelope"></i><a href="#">sales@infowaynic.com</a></li>
                            <li><i class="icon-globe2"></i><a href="#">www.infowaynic.com</a></li>
                        </ul>
                        <!--<h3 class="section-title">Connect with Us</h3>-->
                        <ul class="social-media">
                            <li><a href="#" class="facebook"><i class="icon-facebook"></i></a></li>
                            <li><a href="#" class="twitter"><i class="icon-twitter"></i></a></li>
                            <li><a href="#" class="dribbble"><i class="icon-dribbble"></i></a></li>
                            <li><a href="#" class="github"><i class="icon-github-alt"></i></a></li>
                        </ul>
                    </div>
                    <div class="col-md-4 to-animate">
                        <h3 class="section-title">Drop us a line</h3>
                        <form class="contact-form">
                            <div class="form-group">
                                <label for="name" class="sr-only">Name</label>
                                <input type="name" class="form-control" id="name" placeholder="Name">
                            </div>
                            <div class="form-group">
                                <label for="email" class="sr-only">Email</label>
                                <input type="email" class="form-control" id="email" placeholder="Email">
                            </div>
                            <div class="form-group">
                                <label for="message" class="sr-only">Message</label>
                                <textarea class="form-control" id="message" rows="4" placeholder="Message"></textarea>
                            </div>
                            <div class="form-group">
                                <input type="submit" id="btn-submit" class="btn btn-send-message btn-md" value="Send Message">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- jQuery -->
        <script src="{{asset('assets/website')}}/js/jquery.min.js"></script>
        <!-- jQuery Easing -->
        <script src="{{asset('assets/website')}}/js/jquery.easing.1.3.js"></script>
        <!-- Bootstrap -->
        <script src="{{asset('assets/website')}}/js/bootstrap.min.js"></script>
        <!-- Waypoints -->
        <script src="{{asset('assets/website')}}/js/jquery.waypoints.min.js"></script>
        <!-- Stellar Parallax -->
        <script src="{{asset('assets/website')}}/js/jquery.stellar.min.js"></script>
        <!-- Owl Carousel -->
        <script src="{{asset('assets/website')}}/js/owl.carousel.min.js"></script>
        <!-- Counters -->
        <script src="{{asset('assets/website')}}/js/jquery.countTo.js"></script>
        <!-- Main JS (Do not remove) -->
        <script src="{{asset('assets/website')}}/js/main.js"></script>

    </body>
</html>


<?php
if (Request::segment(1) == 'admin') {
    ?>
    @include('admin.errors.404')
    <?php
    exit;
}
?>
@extends('layouts.app')
@section('content')
<div class="container">
    <hr/>
    <h1>Page Not Found</h1>
    <p>Opppps, You are trying to access wrong page.</p>
</div>
@endsection

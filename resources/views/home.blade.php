@extends('layouts.app')

@section('content')

<section id="fh5co-home" data-section="home">
    <div id="home-carousel" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <!--<ol class="carousel-indicators">
            <li data-target="#home-carousel" data-slide-to="0" class="active"></li>
            <li data-target="#home-carousel" data-slide-to="1"></li>
            <li data-target="#home-carousel" data-slide-to="2"></li>
            <li data-target="#home-carousel" data-slide-to="3"></li>
            <li data-target="#home-carousel" data-slide-to="4"></li>
        </ol>-->

        <!-- Wrapper for slides -->
        <div class="carousel-inner">
            <div class="item active">
                <img src="{{asset('assets')}}/images/slider/vision.png" alt="vision">
            </div>

            <div class="item">
                <img src="{{asset('assets')}}/images/slider/ideas.png" alt="ideas">
            </div>

            <div class="item">
                <img src="{{asset('assets')}}/images/slider/teamwork.png" alt="teamwork">
            </div>

            <div class="item">
                <img src="{{asset('assets')}}/images/slider/on_track.png" alt="on_track">
            </div>

            <div class="item">
                <img src="{{asset('assets')}}/images/slider/success.png" alt="success">
            </div>  
        </div>

        <!-- Left and right controls -->
        <a class="left carousel-control" href="#home-carousel" data-slide="prev">
            <!--<span class="glyphicon glyphicon-chevron-left"></span>
            <span class="sr-only">Previous</span>-->
        </a>
        <a class="right carousel-control" href="#home-carousel" data-slide="next">
            <!--<span class="glyphicon glyphicon-chevron-right"></span>
            <span class="sr-only">Next</span>-->
        </a>
    </div>
</section>
<section>
    <div class="getting-started getting-started-1" style="background: #ffffff;">
        <div class="col-md-8 getting-grid">
            <div class="desc">
                <h2 style="color: #eb4b31;">Monitor <span>performance</span> of your team</h2>
                <p style="color: #8ec2d7;">LogMyDay provides you with a great control over your team’s work. </p>
                <ul class="list-monitor-performance" style="color: #235882;">
                    <li>Bring together timelines of your team on one screen.</li>
                    <li>Track tasks assigned to team members.</li>
                    <li>See an average work intensity of any employee through a day.</li>
                    <li>Edit time records for any team member, if necessary.</li>
                </ul>
            </div>
        </div>
        <div class="col-md-4">
            <div class="pull-right">
                <img src="{{asset('assets')}}/images/banners/daily_routine.jpg" alt="user" class="pull-right" style="max-height: 400px;">
            </div>
        </div>
    </div>
</section>

<section id="fh5co-services" data-section="services">
    <div class="fh5co-services">
        <div class="container">
            <div class="row">
                <div class="col-md-12 section-heading text-center" style="padding-bottom: 0px;">
                    <h2 class="to-animate"><span>We Offer Services</span></h2>
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2 subtext">
                            <h3 class="to-animate">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove. </h3>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 text-center">
                    <div class="box-services">
                        <div class="icon to-animate">
                            <span><i class="icon-chemistry"></i></span>
                        </div>
                        <div class="fh5co-post to-animate">
                            <h3>Super simple time tracking</h3>
                            <p>Just push a button to start your timer - it’s that simple. Filling timesheets has never been closer to fun.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 text-center">
                    <div class="box-services">
                        <div class="icon to-animate">
                            <span><i class="icon-trophy"></i></span>
                        </div>
                        <div class="fh5co-post to-animate">
                            <h3>Flexible and powerful reporting</h3>
                            <p>LogMyDay has tons of ways for breaking down your data and getting the info to you.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 text-center">
                    <div class="box-services">
                        <div class="icon to-animate">
                            <span><i class="icon-people"></i></span>
                        </div>
                        <div class="fh5co-post to-animate">
                            <h3>Works everywhere</h3>
                            <p>You can use LogMyDay on all your devices.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 text-center">
                    <div class="box-services">
                        <div class="icon to-animate">
                            <span><i class="icon-key"></i></span>
                        </div>
                        <div class="fh5co-post to-animate">
                            <h3>Get help any place, any time</h3>
                            <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="fh5co-testimony" class="fh5co-testimony-bg-color" data-section="testimony">
    <div class="container">
        <div class="row">
            <div class="section-heading text-center" style="padding-bottom: 0px;">
                <h2 class="to-animate"><span>Our Fans</span></h2>
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 subtext">
                        <h3 class="to-animate" style="padding-bottom: 0px;">We see our customers as invited guests to a party, and we are the hosts. It's our job every day to make every important aspect of the customer experience a little bit better.</h3>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="pull-left">
                    <img src="{{asset('assets')}}/images/banners/clients.jpg" alt="user" class="pull-right" style="max-height: 400px;">
                </div>
            </div>
            <div class="col-md-8" style="padding-top: 50px;">
                <div class="wrap-testimony">
                    <div class="owl-carousel-fullwidth">
                        <div class="item">
                            <div class="testimony-slide active text-center">
                                <figure>
                                    <img src="{{asset('assets')}}/images/fan/shraddha_halpati.png" alt="user">
                                </figure>
                                <span>Shraddha Halpati <a href="#" class="twitter">Motifworks</a></span>
                                <blockquote>
                                    <p>&ldquo;It’s always hard to predict how much time a project is going to take. Having stats on how much time different things have taken in the past is great for getting accurate predictions for the future.&rdquo;</p>
                                </blockquote>
                            </div>
                        </div>
                        <div class="item">
                            <div class="testimony-slide active text-center">
                                <figure>
                                    <img src="{{asset('assets')}}/images/fan/amar_parnate.png" alt="user">
                                </figure>
                                <span>Amar Parnate <a href="#" class="twitter">Motifworks</a></span>
                                <blockquote>
                                    <p>&ldquo;We’re using LogMyDay to analyze the profitability of the projects we do for our clients, and for tracking our internal ROI. But sometimes we also show the data to our clients - that makes negotiations a lot easier!&rdquo;</p>
                                </blockquote>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section>
    <div class="getting-started getting-started-1">
        <div class="col-md-8 getting-grid">
            <div class="desc">
                <h2>Monitor <span>performance</span> of your team</h2>
                <p>LogMyDay provides you with a great control over your team’s work. </p>
                <ul class="list-monitor-performance">
                    <li>Bring together timelines of your team on one screen.</li>
                    <li>Track tasks assigned to team members.</li>
                    <li>See an average work intensity of any employee through a day.</li>
                    <li>Edit time records for any team member, if necessary.</li>
                </ul>
            </div>
        </div>
        <div class="col-md-4">
            <div class="pull-right">
                <img src="{{asset('assets')}}/images/banners/personal_report.jpg" alt="user" class="pull-right" style="max-height: 400px;">
            </div>
        </div>
    </div>
</section>


<section id="fh5co-team" class="fh5co-bg-color" data-section="team">
    <div class="fh5co-team">
        <div class="container">
            <div class="row">
                <div class="col-md-12 section-heading text-center">
                    <h2 class="to-animate"><span>The Dumb Guys</span></h2>
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2 subtext">
                            <h3 class="to-animate">
                                LogMyDay is kind of a weird family of very different people from all corners of the great INDIA. One thing we all share is a belief that software should empower people, and not get in their way. We also share memes, but to get in on that you’ll have to join our team.
                            </h3>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-bottom: 100px;">
                <div class="col-md-4">
                    <div class="team-box text-center to-animate-2">
                        <div class="user"><img class="img-reponsive" src="{{asset('assets')}}/images/team/rahul_katba.png" alt="Rahul Katba"></div>
                        <h3>Rahul Katba</h3>
                        <span class="position">Chief Troublemaker</span>
                        <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
                        <ul class="social-media">
                            <li><a href="#" class="facebook"><i class="icon-facebook"></i></a></li>
                            <li><a href="#" class="linkedin"><i class="icon-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="team-box text-center to-animate-2">
                        <div class="user"><img class="img-reponsive" src="{{asset('assets')}}/images/team/naru_keer.png" alt="Naru Keer"></div>
                        <h3>Naru Keer</h3>
                        <span class="position">Chief Cheerleader</span>
                        <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
                        <ul class="social-media">
                            <li><a href="#" class="facebook"><i class="icon-facebook"></i></a></li>
                            <li><a href="#" class="linkedin"><i class="icon-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="team-box text-center to-animate-2">
                        <div class="user"><img class="img-reponsive" src="{{asset('assets')}}/images/team/anubhav_seal.png" alt="Anubhav Seal"></div>
                        <h3>Anubhav Seal</h3>
                        <span class="position">Chief Curator</span>
                        <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
                        <ul class="social-media">
                            <li><a href="#" class="facebook"><i class="icon-facebook"></i></a></li>
                            <li><a href="#" class="linkedin"><i class="icon-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="team-box text-center to-animate-2">
                        <div class="user"><img class="img-reponsive" src="{{asset('assets')}}/images/team/akansha_deshmukh.png" alt="Akansha Deshmukh"></div>
                        <h3>Akansha Deshmukh</h3>
                        <span class="position">Visual Curator</span>
                        <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
                        <ul class="social-media">
                            <li><a href="#" class="facebook"><i class="icon-facebook"></i></a></li>
                            <li><a href="#" class="linkedin"><i class="icon-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="team-box text-center to-animate-2">
                        <div class="user"><img class="img-reponsive" src="{{asset('assets')}}/images/team/mridul_chatterjee.png" alt="Mridul Chatterjee"></div>
                        <h3>Mridul Chatterjee</h3>
                        <span class="position">Arts and Crafts Designer</span>
                        <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
                        <ul class="social-media">
                            <li><a href="#" class="facebook"><i class="icon-facebook"></i></a></li>
                            <li><a href="#" class="linkedin"><i class="icon-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="team-box text-center to-animate-2">
                        <div class="user"><img class="img-reponsive" src="{{asset('assets')}}/images/team/kundan_roy.png" alt="Kundan Roy"></div>
                        <h3>Kundan Roy</h3>
                        <span class="position">Sous Chef</span>
                        <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
                        <ul class="social-media">
                            <li><a href="#" class="facebook"><i class="icon-facebook"></i></a></li>
                            <li><a href="#" class="linkedin"><i class="icon-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
